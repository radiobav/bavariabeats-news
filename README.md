# BavariaBeats News

These script are responsible for playing the news at BavariaBeats.
To use this script you will need the file **refresh_news.sh** and the program [rss-folder](https://gitlab.com/radiobav/rss-folder)

## refresh_news.sh
This script fetches the news from an Onlineservice called jingel-service.de .
You have to call it periodically e.g. a cronjob so that the news get updated.
The script does have two variables:
- SAVE_FILE_PATH: here you give the path where the downloaded mp3 files should be saved.
- DOWNLOAD_LINK: Here you enter the "direct download link" you got from jiingel-service.de
On execution the script is fetching a news file from the service and saves it as "news($date).mp3" in the "audio" folder.
Then the script calls the *rss-folder* program, the program binary must be placed in the workdir of the script.

## Access the news with Libretime
The folder SAVE_FILE_PATH where the downloaded mp3s and the rss-folder's feed.rss are stored must be accessible through the internet.
You may want to add some Http-Basic-Authentication.

Then you can add the url to the folder (depending on your domain and webserver-config) as a Podcast to LibreTime.
Depending on your server you need to change the lines 11-13 in the **refresh_news.sh** script.

How to configure LibreTime to play the latest news-episode can be found in this Video
[LibreTime 101: How to schedule a podcast to play the newest episode on an automatic basis](https://www.youtube.com/watch?v=g-4UcD8qvR8)
[mirror](https://redirect.invidious.io/watch?v=g-4UcD8qvR8)

### Exsample cron entry
Download a new news file every morning at 3:40
```bash

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name  command to be executed
  40 3  *  *  * root /usr/bin/bash /opt/libretime/radiobav-news/refresh_news.sh
```
