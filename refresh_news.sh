#!/bin/bash

SAVE_FILE_PATH=/opt/news/audio/
DOWNLOAD_LINK=InserLinkeHere

#Download new news file from the server
wget -q -O "$SAVE_FILE_PATH/new.mp3" "$DOWNLOAD_LINK"

NEW_HASH=$(sha256sum "$SAVE_FILE_PATH/new.mp3" | cut -d " " -f1)
LAST_HASH=$(cat /opt/news/last_hash.txt)

if [ "$LAST_HASH" = "$NEW_HASH" ]; then
	echo "Hash are the same not saving downloaded file"
	
	rm -f "$SAVE_FILE_PATH/new.mp3"
else	
	#Copt the new file to the destination
	mv "$SAVE_FILE_PATH/new.mp3" "$SAVE_FILE_PATH/news$(date +"%d.%m.%y-%H-%M-%S").mp3"
	
	# Remove the old feed.rss file
	rm -f "$SAVE_FILE_PATH/feed.rss"

	#Create a new feed.rss file to include the added audio file
	/opt/news/rss-folder \
	--folder "$SAVE_FILE_PATH" \
	--filename "feed.rss" \
	--feed-name "Bavariabeats News" \
	--hostname "news.bavariabeats.de" \
	--protocol "https"

	#Write hash to file for next try
	echo "$NEW_HASH" > /opt/news/last_hash.txt
fi

#Remove all files that are older than 10 Days
find "$SAVE_FILE_PATH" -mtime +10 -type f -delete
